package lbm

import (
	"testing"
)

func TestConfig(t *testing.T) {
	err := Config("default.xml")
	if err != nil {
		t.Error(err)
	}
}

func TestNewContext(t *testing.T) {
	_, err := NewContext()
	if err != nil {
		t.Error(err)
	}
}

func TestNewReceiver(t *testing.T) {
	ctx, err := NewContext()
	if err != nil {
		t.Error("Context failed", err)
	}
	inc:=make(chan *Message, 1000)
	_, err = ctx.NewReceiver("/tmp",inc)
	if err != nil {
		t.Error("receiver failed", err)
	}
}
func TestNewWildcardReceiver(t *testing.T) {
	ctx, err := NewContext()
	if err != nil {
		t.Error("Context failed", err)
	}
	inc:=make(chan *Message)
	_, err = ctx.NewWildcardReceiver(".*",inc)
	if err != nil {
		t.Error("wildcard receiver failed", err)
	}

}

func TestNewSender(t *testing.T) {
	ctx, err := NewContext()
	if err != nil {
		t.Error("Context failed", err)
	}
	_, err = ctx.NewSender("/tmp")
	if err != nil {
		t.Error("sender failed", err)
	}

}
