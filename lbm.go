package lbm

/*
#cgo LDFLAGS: -L/opt/UMS/Linux/lib -llbm -lm
#cgo CFLAGS: 	-I/opt/UMS/Linux/include
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <lbm/lbm.h>
#
extern lbm_rcv_cb_proc rcv_callback;
*/
import "C"

// cgo LDFLAGS: -L/opt/UMS/Linux/lib -llbm -lm
// cgo LDFLAGS: /opt/UMS/Linux/lib/liblbm.a
import (
	"errors"
	_ "expvar"
	"fmt"
	"unsafe"
)

var rcvCallback = lbm_receiver_callback

type rcvWrapper struct {
	rcvChan chan *Message
}

//export lbm_receiver_callback
func lbm_receiver_callback(rcv unsafe.Pointer, msg unsafe.Pointer, clientd unsafe.Pointer) int {
	rcvWrap := (*rcvWrapper)(clientd)
	var rawmsg = (*C.struct_lbm_msg_t_stct)(msg)
	data := C.GoBytes(unsafe.Pointer(rawmsg.data), (C.int)(rawmsg.len))
	flags := (MsgFlag)(rawmsg.flags)
	msgtype := (MsgType)(rawmsg._type)
	topic := C.GoString(&rawmsg.topic_name[0])
	source := C.GoString(&rawmsg.source[0])
	datamsg := Message{flags, msgtype, data, topic, source}
	rcvWrap.rcvChan <- &datamsg
	return 0
}

type MsgType int

const (
	MSG_DATA                                               MsgType = C.LBM_MSG_DATA
	MSG_BOS                                                        = C.LBM_MSG_BOS
	MSG_EOS                                                        = C.LBM_MSG_EOS
	MSG_REQUEST                                                    = C.LBM_MSG_REQUEST
	MSG_RESPONSE                                                   = C.LBM_MSG_RESPONSE
	MSG_UNRECOVERABLE_LOSS                                         = C.LBM_MSG_UNRECOVERABLE_LOSS
	MSG_UNRECOVERABLE_LOSS_BURST                                   = C.LBM_MSG_UNRECOVERABLE_LOSS_BURST
	MSG_NO_SOURCE_NOTIFICATION                                     = C.LBM_MSG_NO_SOURCE_NOTIFICATION
	MSG_UME_REG_ERROR                                              = C.LBM_MSG_UME_REGISTRATION_ERROR
	MSG_UME_REG_SUCCESS                                            = C.LBM_MSG_UME_REGISTRATION_SUCCESS
	MSG_UME_REG_CHANGE                                             = C.LBM_MSG_UME_REGISTRATION_CHANGE
	MSG_UME_REG_SUCCESS_EX                                         = C.LBM_MSG_UME_REGISTRATION_SUCCESS_EX
	MSG_UME_DEREG_SUCCESS_EX                                       = C.LBM_MSG_UME_DEREGISTRATION_SUCCESS_EX
	MSG_UME_DEREG_COMPLETE_EX                                      = C.LBM_MSG_UME_DEREGISTRATION_COMPLETE_EX
	MSG_UMQ_REG_ERROR                                              = C.LBM_MSG_UMQ_REGISTRATION_ERROR
	MSG_UMQ_REG_COMPLETE_EX                                        = C.LBM_MSG_UMQ_REGISTRATION_COMPLETE_EX
	MSG_UMQ_DEREG_COMPLETE_EX                                      = C.LBM_MSG_UMQ_DEREGISTRATION_COMPLETE_EX
	MSG_UMQ_INDEX_ASSIGNMENT_ELIGIBILITY_ERROR                     = C.LBM_MSG_UMQ_INDEX_ASSIGNMENT_ELIGIBILITY_ERROR
	MSG_UMQ_INDEX_ASSIGNMENT_ELIGIBILITY_START_COMPLETE_EX         = C.LBM_MSG_UMQ_INDEX_ASSIGNMENT_ELIGIBILITY_START_COMPLETE_EX
	MSG_UMQ_INDEX_ASSIGNMENT_ELIGIBILITY_STOP_COMPLETE_EX          = C.LBM_MSG_UMQ_INDEX_ASSIGNMENT_ELIGIBILITY_STOP_COMPLETE_EX
	MSG_UMQ_INDEX_ASSIGNED_EX                                      = C.LBM_MSG_UMQ_INDEX_ASSIGNED_EX
	MSG_UMQ_INDEX_RELEASED_EX                                      = C.LBM_MSG_UMQ_INDEX_RELEASED_EX
	MSG_UMQ_INDEX_ASSIGNMENT_ERROR                                 = C.LBM_MSG_UMQ_INDEX_ASSIGNMENT_ERROR
	MSG_HF_RESET                                                   = C.LBM_MSG_HF_RESET
)

var msgTypeLookup = map[MsgType]string{MSG_DATA: `MSG_DATA`, MSG_BOS: `MSG_BOS`, MSG_EOS: `MSG_EOS`, MSG_REQUEST: `MSG_REQUEST`, MSG_RESPONSE: `MSG_RESPONSE`,
	MSG_UNRECOVERABLE_LOSS: `MSG_UNRECOVERABLE_LOSS`, MSG_UNRECOVERABLE_LOSS_BURST: `MSG_UNRECOVERABLE_LOSS_BURST`, MSG_NO_SOURCE_NOTIFICATION: `MSG_NO_SOURCE_NOTIFICATION`,
}

func (m MsgType) String() string {
	return msgTypeLookup[m]
}

type MsgFlag int

const (
	FLAG_START_BATCH     = C.LBM_MSG_FLAG_START_BATCH
	FLAG_END_BATCH       = C.LBM_MSG_FLAG_END_BATCH
	FLAG_HF_PASS_THROUGH = C.LBM_MSG_FLAG_HF_PASS_THROUGH
	FLAG_RETRANSMIT      = C.LBM_MSG_FLAG_RETRANSMIT
	FLAG_IMMEDIATE       = C.LBM_MSG_FLAG_IMMEDIATE
	FLAG_TOPICLESS       = C.LBM_MSG_FLAG_TOPICLESS
	FLAG_HF_32           = C.LBM_MSG_FLAG_HF_32
	FLAG_HF_64           = C.LBM_MSG_FLAG_HF_64
)

var msgFlagLookup = map[MsgFlag]string{FLAG_START_BATCH: `FLAG_START_BATCH`, FLAG_END_BATCH: `FLAG_END_BATCH`, FLAG_HF_PASS_THROUGH: `FLAG_HF_PASS_THROUGH`, FLAG_RETRANSMIT: `FLAG_RETRANSMIT`,
	FLAG_IMMEDIATE: `FLAG_IMMEDIATE`, FLAG_TOPICLESS: `FLAG_TOPICLESS`, FLAG_HF_32: `FLAG_HF_32`, FLAG_HF_64: `FLAG_HF_64`}

func (f MsgFlag) String() string {
	return msgFlagLookup[f]
}

type Message struct {
	Flags   MsgFlag
	MsgType MsgType
	Data    []byte
	Topic   string
	Source  string
}

type Context struct {
	ctx *C.lbm_context_t
}

type Receiver struct {
	rcv *C.lbm_rcv_t
}

type WildcardReceiver struct {
	wrcv *C.lbm_wildcard_rcv_t
}

type Sender struct {
	snd *C.lbm_src_t
}

func (src *Sender) Send(msg []byte) error {
	var data = (*C.char)(unsafe.Pointer(&msg[0]))
	rv := C.lbm_src_send(src.snd, data, C.size_t(len(msg)), C.LBM_SRC_BLOCK)
	if rv != C.LBM_OK {
		return errors.New(errorMsg())
	}
	return nil
}

func Version() string {
	v := C.lbm_version()
	return C.GoString(v)
}

func errorMsg() string {
	v := C.lbm_errmsg()
	return C.GoString(v)
}

func NewContext() (*Context, error) {
	var ctx *C.lbm_context_t
	rv := C.lbm_context_create(&ctx, nil, nil, nil)
	if rv != C.LBM_OK {
		return nil, errors.New(errorMsg())
	}
	return &Context{ctx}, nil
}

func (context *Context) NewReceiver(topicname string, incoming chan *Message) (*Receiver, error) {
	var rcv *C.lbm_rcv_t
	var topic *C.lbm_topic_t
	var rv C.int
	rv = C.lbm_rcv_topic_lookup(&topic, context.ctx, C.CString(topicname), nil)
	if rv != C.LBM_OK {
		return nil, errors.New(errorMsg())
	}

	fmt.Println("calling create rcv")
	rv = C.lbm_rcv_create(&rcv, context.ctx, topic, C.rcv_callback, unsafe.Pointer(&incoming), nil)
	if rv != C.LBM_OK {
		return nil, errors.New(errorMsg())
	}
	return &Receiver{rcv}, nil
}

var wildcards = make(map[*WildcardReceiver]*rcvWrapper)

func (context *Context) NewWildcardReceiver(pattern string, incoming chan *Message) (*WildcardReceiver, error) {
	var rcv *C.lbm_wildcard_rcv_t
	var rv C.int
	chanWrap := &rcvWrapper{incoming}
	rv = C.lbm_wildcard_rcv_create(&rcv, context.ctx, C.CString(pattern), nil, nil, C.rcv_callback, unsafe.Pointer(chanWrap), nil)
	if rv != C.LBM_OK {
		return nil, errors.New(errorMsg())
	}
	wrcv := &WildcardReceiver{rcv}
	wildcards[wrcv] = chanWrap
	return wrcv, nil
}
func (context *Context) NewSender(topicname string) (*Sender, error) {
	var snd *C.lbm_src_t
	var topic *C.lbm_topic_t
	var rv C.int
	rv = C.lbm_src_topic_alloc(&topic, context.ctx, C.CString(topicname), nil)
	if rv != C.LBM_OK {
		return nil, errors.New(errorMsg())
	}

	rv = C.lbm_src_create(&snd, context.ctx, topic, nil, nil, nil)
	if rv != C.LBM_OK {
		return nil, errors.New(errorMsg())
	}
	return &Sender{snd}, nil
}

func Config(filename string) error {
	fname := C.CString(filename)
	rv := C.lbm_config(fname)
	if rv != 0 {
		return errors.New(errorMsg())
	}
	return nil
}
